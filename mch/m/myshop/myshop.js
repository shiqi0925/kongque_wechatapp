var api = require('../../../api.js');
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        app.pageOnLoad(this);
        var page = this;
        wx.showLoading({
            title: '加载中',
        });
        app.request({
            url: api.mch.user.myshop,
            success: function (res) {
                wx.hideLoading();
                if (res.code == 0) {
                    page.setData(res.data);
                }
                if (res.code == -2) {
                    wx.redirectTo({
                        url: '/mch/apply/apply',
                    });
                }
            }
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        app.pageOnReady(this);
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        app.pageOnShow(this);
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        app.pageOnHide(this);
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        app.pageOnUnload(this);
    },

    navigatorSubmit: function (e) {
        console.log(e);
        app.request({
            url: api.user.save_form_id + "&form_id=" + e.detail.formId,
        });
        wx.navigateTo({
            url: e.detail.value.url,
        });
    },

    showPcUrl: function () {
        var page = this;
        page.setData({
            show_pc_url: true,
        });
    },

    hidePcUrl: function () {
        var page = this;
        page.setData({
            show_pc_url: false,
        });
    },

    copyPcUrl: function () {
        var page = this;
        wx.setClipboardData({
            data: page.data.pc_url,
            success: function (res) {
                page.showToast({
                    title: '内容已复制',
                });
            },
        });
    },

});