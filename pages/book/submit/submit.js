// pages/book/submit/submit.js
var api = require('../../../api.js');
var utils = require('../../../utils/utils.js');
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
      currDate:'',
      currOption:'',
      form_list1:[],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        app.pageOnLoad(this);
        this.getPreview(options);
 

        
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        app.pageOnShow(this);
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    /**
     * 复选
     */
    checkboxChange: function (e) {
        console.log(e.target.dataset.id);
        var page = this;
        var pid = e.target.dataset.pid;
        var id = e.target.dataset.id;
        var form_list = page.data.form_list;
        var is_selected = form_list[pid].default[id]['selected'];
        if (is_selected == true) {
            form_list[pid].default[id]['selected'] = false;
        } else {
            form_list[pid].default[id]['selected'] = true;
        }
        page.setData({
            form_list: form_list,
        });
    },
    /**
     * 单选
     */
    radioChange: function (e) {
        var page = this;
        var pid = e.target.dataset.pid;
        var form_list = page.data.form_list;
      console.log("e:", e);
      console.log("e:", e.target.dataset.ischoice);
        //data-isChoice



      if (e.target.dataset.ischoice==true){
        console.log('已经被预约');
        wx.showModal({
          title: '提示',
          content: '已经被预约',
          showCancel:false,
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }  
          }
        })
      } else if (e.target.dataset.ischoice == false){
        for (var i in form_list[pid].default) {
          if (e.target.dataset.id == i) {
            form_list[pid].default[i]['selected'] = true;
          } else {
            form_list[pid].default[i]['selected'] = false;
          }
        }
        page.setData({
          form_list: form_list,
        });
      }else{
        console.log("data-name:", e.target.dataset.name) 
        console.log("this.data.currDate:", this.data.currDate)
        this.getTimeState(this.data.currDate, e.target.dataset.name)

        for (var i in form_list[pid].default) {
          if (e.target.dataset.id == i) {
            form_list[pid].default[i]['selected'] = true;
          } else {
            form_list[pid].default[i]['selected'] = false;
          }
        }
        page.setData({
          form_list: form_list,
          currOption: e.target.dataset.name
        });

      }


    },
    /**
     * input 改变
     */
    inputChenge: function (e) {
      console.log("inputChenge:",e);
        var page = this;
        var id = e.target.dataset.id;
      console.log("e.target.dataset.date:", e.detail.value);
        var form_list = page.data.form_list;
        form_list[id].default = e.detail.value;
        page.setData({
            form_list: form_list,
        });

      this.getTimeState(e.detail.value, this.data.currOption)


    },

    /**
     * 获取数据
     * getsubmit_preview
     */
    getTimeState: function (item,option) {
      // console.log("item:", item); 
      // console.log("option:", option); 
      // console.log("orderForm:", this.data.orderForm); 
      console.log(this.data.form_list[2].default);
    
      
     // array.filter(function (currentValue, index, arr){}, thisValue)
      var orderId=[];
      for (let i of this.data.orderForm) {
        // console.log(i.value);
        if (item == i.value) {
          console.log('是当前日期');
          orderId.push(i.order_id); 
        }
      }
      //得到当前日期的订单
      var newOrderId=[]
      console.log("orderId:", orderId);
      for (let i of this.data.orderForm) {
        // console.log(i.value);
        if (i.key == '美容师' && i.value==option) {
          if (orderId.indexOf(i.order_id)>-1){
            newOrderId.push(i.order_id); 
          }
        }
      }
      //得到美容师的订单
      console.log("newOrderId:", newOrderId);
      var timeList=[];
      for (let i of this.data.orderForm) {
        // console.log(i.value);
        if (i.key == '预约时间') {
          if (newOrderId.indexOf(i.order_id) > -1) {
            timeList.push(i.value);
          }
        }
      }
      console.log("timeList:", timeList);
  //得到当前日期的美容师的订单
      console.log("form_list time:",this.data.form_list[2]['default'])
      var form_list_copy = this.data.form_list;
      var normalTimeList = form_list_copy[2]['default'];
     // console.log('norma值' + JSON.stringify(normalTimeList));
     var k=0;
      for (let i of normalTimeList) {
        console.log("k:",k);
        // console.log(i.value);
        if (timeList.indexOf(i.name)>-1) {
           form_list_copy[2]['default'][k]['isChoice']=true;
        }else{
          form_list_copy[2]['default'][k]['isChoice'] = false;
        }
        form_list_copy[2]['default'][k]['selected'] = false;
        k++;
      }
      console.log("form_list_copy:", form_list_copy);
      this.setData({
        form_list: form_list_copy
      })

      //console.log("timeList:", timeList);
    },
    getPreview: function (e) {
        var page = this;
        console.log("this:",this);
        var gid = e.id;
        wx.showLoading({
            title: "正在加载",
            mask: true,
        });
        // wx.showNavigationBarLoading();
        app.request({
            url: api.book.submit_preview,
            method: "get",
            data: { gid: gid },
            success: function (res) {
                if (res.code == 0) {
                    for (var i in res.data.form_list) {
                        if (res.data.form_list[i].type == 'date') {
                            res.data.form_list[i].default = res.data.form_list[i].default ? res.data.form_list[i].default : utils.formatData(new Date);
                        }
                        if (res.data.form_list[i].type == 'time') {
                            res.data.form_list[i].default = res.data.form_list[i].default ? res.data.form_list[i].default : '00:00';
                        }
                    }
                    page.setData({
                        goods: res.data.goods,
                        form_list: res.data.form_list,
                        orderForm: res.data.orderForm,
                        form_list1: res.data.form_list,
                        currDate: res.data.form_list[0]['default'],
                        currOption: res.data.form_list[1]['default'][0]['name'],
                    });

                  console.log("currDate:", page.data.currDate);
                  console.log("currOption:", page.data.currOption);
                    // for (let i of Array.from(this.data.form_list)) {
                    //   console.log(i);
                    // }
                   // var currDate
                  page.getTimeState(page.data.currDate, page.data.currOption);



                } else {
                    wx.showModal({
                        title: '提示',
                        content: res.msg,
                        showCancel: false,
                        success: function (res) {
                            if (res.confirm) {
                                wx.redirectTo({
                                    url: '/pages/book/index/index'
                                });
                            }
                        }
                    });
                }
            },
            complete: function (res) {
                setTimeout(function () {
                    // 延长一秒取消加载动画
                    wx.hideLoading();
                }, 1000);
            }
        });
    },
    submit: function (e) {
        var form_id = e.detail.formId;
        var page = this;
        var gid = page.data.goods.id;
        var form_list = JSON.stringify(page.data.form_list);
        console.log(form_list);
        wx.showLoading({
            title: "正在提交",
            mask: true,
        });
        // wx.showNavigationBarLoading();
        app.request({
            url: api.book.submit,
            method: "post",
            data: { gid: gid, form_list: form_list, form_id: form_id },
            success: function (res) {
                if (res.code == 0) {
                    if (res.type == 1) {
                        // 免费
                        wx.redirectTo({
                            url: "/pages/book/order/order?status=1",
                        });
                    } else {
                        wx.showLoading({
                            title: "正在提交",
                            mask: true,
                        });
                        //发起支付
                        wx.requestPayment({
                            timeStamp: res.data.timeStamp,
                            nonceStr: res.data.nonceStr,
                            package: res.data.package,
                            signType: res.data.signType,
                            paySign: res.data.paySign,
                            success: function (e) {
                                wx.redirectTo({
                                    url: "/pages/book/order/order?status=1",
                                });
                            },
                            fail: function (e) {
                            },
                            complete: function (e) {
                                setTimeout(function () {
                                    // 延长一秒取消加载动画
                                    wx.hideLoading();
                                }, 1000);
                                if (e.errMsg == "requestPayment:fail" || e.errMsg == "requestPayment:fail cancel") {//支付失败转到待支付订单列表
                                    wx.showModal({
                                        title: "提示",
                                        content: "订单尚未支付",
                                        showCancel: false,
                                        confirmText: "确认",
                                        success: function (res) {
                                            if (res.confirm) {
                                                wx.redirectTo({
                                                    url: "/pages/book/order/order?status=0",
                                                });
                                            }
                                        }
                                    });
                                    return;
                                }
                                if (e.errMsg == "requestPayment:ok") {
                                    return;
                                }
                                wx.redirectTo({
                                    url: "/pages/book/order/order?status=-1",
                                });
                            },
                        });
                        return;
                    }
                } else {
                    wx.showModal({
                        title: '提示',
                        content: res.msg,
                        showCancel: false,
                        success: function (res) {
                            // if (res.confirm) {
                            //     wx.redirectTo({
                            //         url: '/pages/book/index/index'
                            //     });
                            // }
                        }
                    });
                }
            },
            complete: function (res) {
                setTimeout(function () {
                    // 延长一秒取消加载动画
                    wx.hideLoading();
                }, 1000);
            }
        });
    },
    uploadImg: function (e) {
        var page = this;
        var index = e.currentTarget.dataset.id;
        var form_list = page.data.form_list;
        app.uploader.upload({
            start: function () {
                wx.showLoading({
                    title: '正在上传',
                    mask: true,
                });
            },
            success: function (res) {
                if (res.code == 0) {
                    form_list[index].default = res.data.url
                    page.setData({
                        form_list: form_list,
                    });
                } else {
                    page.showToast({
                        title: res.msg,
                    });
                }
            },
            error: function (e) {
                console.log(e);
                page.showToast({
                    title: e,
                });
            },
            complete: function () {
                wx.hideLoading();
            }
        })
    }
})