// pages/shop-detail/shop-detail.js
var api = require('../../api.js');
var utils = require('../../utils.js');
var app = getApp();
var is_no_more = false;
var WxParse = require('../../wxParse/wxParse.js');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        score: [1, 2, 3, 4, 5],
        cat_id: "1",
        page: 1,
        cat_list: [],
        goods_list: [],
        sort: 0,
        sort_type: -1,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        app.pageOnLoad(this);
        var page = this;

        var user_id = options.user_id;
        app.loginBindParent({ parent_id: user_id });
        page.setData({
            shop_id: options.shop_id
        });
        console.log(user_id);
        wx.showLoading({
            title: '加载中',
        });
        app.request({
            url: api.default.shop_detail,
            method: 'GET',
            data: {
                shop_id: options.shop_id
            },
            success: function (res) {
                if (res.code == 0) {
                    console.log("123:"+res.data.shop.cat_id)
                   
                  page.setData({
                    cat_id: res.data.shop.cat_id
                  });
                    page.setData(res.data);
                    var detail = res.data.shop.content ? res.data.shop.content : "<span>暂无信息</span>"
                    WxParse.wxParse("detail", "html", detail, page);
                  var p = page.data.page || 1;
                  app.request({
                    url: api.default.goods_list,
                    data: {
                      cat_id: res.data.shop.cat_id,
                      page: 1
                    },
                    success: function (res) {
                      if (res.code == 0) {
                        if (res.data.list.length == 0)
                          is_no_more = true;
                        page.setData({ page: (p + 1) });
                        page.setData({ goods_list: res.data.list });
                      }
                      page.setData({
                        show_no_data_tip: (page.data.goods_list.length == 0),
                      });
                    },
                    complete: function () {
                      //wx.hideNavigationBarLoading();
                    }
                  });


                } else {
                  console.log(22222)
                    wx.showModal({
                        title: '提示',
                        content: res.msg,
                        showCancel: false,
                        success: function (e) {
                            if (e.confirm) {
                                wx.redirectTo({
                                    url: '/pages/shop/shop',
                                })
                            }
                        }
                    })
                }
            },
            complete: function () {
                wx.hideLoading();
            }
        });
        this.loadData(options);
    },
    // loadData: function (options) {
    //   var page = this;
    //   var cat_list = wx.getStorageSync("cat_list");
    //   var height_bar = "";
    //   if (options.cat_id) {
    //     for (var i in cat_list) {
    //       var in_list = false;
    //       if (cat_list[i].id == options.cat_id) {
    //         cat_list[i].checked = true;
    //         if (cat_list[i].list.length > 0) {
    //           height_bar = "height-bar";
    //         }
    //       }
    //       for (var j in cat_list[i].list) {
    //         if (cat_list[i].list[j].id == options.cat_id) {
    //           cat_list[i].list[j].checked = true;
    //           in_list = true;
    //           height_bar = "height-bar";
    //         }
    //       }
    //       if (in_list)
    //         cat_list[i].checked = true;
    //     }
    //   }
    //   if (options.goods_id) {
    //     var goods_id = options.goods_id
    //   }
    //   page.setData({
    //     cat_list: cat_list,
    //     cat_id: options.cat_id || "",
    //     height_bar: height_bar,
    //     goods_id: goods_id || "",
    //   });
    //   page.reloadGoodsList();

    // },
    reloadGoodsList: function () {
      var page = this;
      is_no_more = false;
      page.setData({
        page: 1,
        goods_list: [],
        show_no_data_tip: false,
      });
      var cat_id = page.data.cat_id || "";
      var p = page.data.page || 1;
      //wx.showNavigationBarLoading();
      app.request({
        url: api.default.goods_list,
        data: {
          cat_id: cat_id,
          page: p,
          sort: page.data.sort,
          sort_type: page.data.sort_type,
          goods_id: page.data.goods_id,
        },
        success: function (res) {
          if (res.code == 0) {
            if (res.data.list.length == 0)
              is_no_more = true;
            page.setData({ page: (p + 1) });
            page.setData({ goods_list: res.data.list });
          }
          page.setData({
            show_no_data_tip: (page.data.goods_list.length == 0),
          });
        },
        complete: function () {
          //wx.hideNavigationBarLoading();
        }
      });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        app.pageOnShow(this);

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    mobile: function () {
        var page = this;
        wx.makePhoneCall({
            phoneNumber: page.data.shop.mobile,
        })
    },
    goto: function () {
        var page = this;
        wx.getSetting({
            success: function (res) {
                if (!res.authSetting['scope.userLocation']) {
                    app.getauth({
                        content: '需要获取您的地理位置授权，请到小程序设置中打开授权！',
                        cancel: false,
                        success: function (res) {
                            if (res.authSetting['scope.userLocation']) {
                                page.location();
                            }
                        }
                    });
                } else {
                    page.location();
                }
            }
        })
    },

    location: function () {
        var page = this;
        var shop = page.data.shop;
        wx.openLocation({
            latitude: parseFloat(shop.latitude),
            longitude: parseFloat(shop.longitude),
            name: shop.name,
            address: shop.address
        })
    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (options) {
        var page = this;
        var user_info = wx.getStorageSync("user_info");
        var shop_id = page.data.shop_id;
        return {
            path: "/pages/shop-detail/shop-detail?shop_id="+shop_id+"&user_id=" + user_info.id,
        };
    },
})